(** Client module *)

(** used to know if the client wants a text or a graphic display *)
type render = Text | Graphic

(** The current render mode, GUI by default *)
val render : render ref

(** represents a request, which will be sent to the server *)
type request = Msg.to_server

(** transform a render type in a string *)
val string_of_render : render -> string

(** initialisation function *)
val init_client : unit -> unit

(** update render *)
val update_render : unit -> render ->  unit

(** wait player input *)
val wait_input : unit -> render -> unit

(** attempt to load a game from a json file, set values in client_state
 * or exit with an error message *)
val load_json_file: string -> unit
